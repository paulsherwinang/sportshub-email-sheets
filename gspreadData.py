import json
import gspread
from oauth2client.client import SignedJwtAssertionCredentials

def authenticate():
    json_key = json.load(open('key.json'));
    scope = ['https://spreadsheets.google.com/feeds'];

    credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope);
    gc = gspread.authorize(credentials);

    return gc;

def print_to_sheet(data):
    gc = authenticate();
    sh = gc.open_by_key("1reyuFOcBc1BC9hSr2YuVo9Tbif6LvTlqXqi4_i5WXv0");
    ws = sh.worksheet('Court Bookings')

    delete_sheet(ws)
    i = 3

    for line in data:
        ws.update_acell('A'+str(i), line[4]);
        ws.update_acell('B'+str(i), line[5]);
        ws.update_acell('C'+str(i), line[3]);
        ws.update_acell('D'+str(i), line[0]);
        i+=1

def delete_sheet(ws):
    cell_list = ws.range('A3:D27')
    for cell in cell_list:
        cell.value = ''
    ws.update_cells(cell_list)
