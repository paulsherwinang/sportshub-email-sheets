#!/usr/bin/env python
import json
import gspreadData
from fabric.api import *

def file_to_sheets():
    with open('schedule.json') as f:
        data = json.load(f)

    gspreadData.print_to_sheet(data)
