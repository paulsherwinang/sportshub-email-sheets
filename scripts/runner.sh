#/!bin/sh

echo "======================================="
echo "EMAIL RUNNER EXECUTED AT: $(date)"
echo "======================================="
cd ~/Documents/sportshub-email-sheets/
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin

npm start
