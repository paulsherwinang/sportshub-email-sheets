var googleAuth = require('google-auth-library');
var readline = require('readline');
var fs = require('fs');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/gmail-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'gmail-nodejs-quickstart.json';

module.exports = {
    authorize: function(credentials) {
        var clientSecret = credentials.installed.client_secret;
        var clientId = credentials.installed.client_id;
        var redirectUrl = credentials.installed.redirect_uris[0];
        var auth = new googleAuth();
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

        return new Promise(
            function(resolve, reject){
                fs.readFile(TOKEN_PATH, function(err, token) {
                    if (err) {
                        reject(oauth2Client);
                    } else {
                        oauth2Client.credentials = JSON.parse(token);
                        resolve(oauth2Client);
                    }
                });
            }
        )
    },
    getNewToken: function(oauth2Client) {
        var authUrl = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES
        });
        console.log('Authorize this app by visiting this url: ', authUrl);
        var rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        return new Promise(
            function(resolve, reject){
                rl.question('Enter the code from that page here: ', function(code) {
                    rl.close();
                    oauth2Client.getToken(code, function(err, token) {
                        if (err) {
                            reject(err);
                            console.log('Error while trying to retrieve access token', err);
                            return;
                        }
                        oauth2Client.credentials = token;
                        storeToken(token);
                        resolve(oauth2Client);
                    });
                });
            }
        )
    }
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {

}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
}
