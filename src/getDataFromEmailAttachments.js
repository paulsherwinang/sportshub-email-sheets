var _ = require('lodash');
var fs = require('fs');
var google = require('googleapis');
var PDFParser = require("../node_modules/pdf2json/pdfparser");

module.exports = function makeGmailApiCall(token){
    google.options({ auth: token });

    return getMessagesList('subject:(Sports Hub Booking Confirmation) filename:pdf newer_than:8d')
                .then(getMessage, function(err){ console.log(err); })
                .then(getAttachment, function(err){ console.log(err); })
                .then(parseBufferToJson, function(err){ console.log(err); })
                .then(parsePdfJsonData, function(err){ console.log(err); });
}

function getMessagesList(params){
    var gmail = google.gmail('v1');

    return new Promise(
        function(resolve, reject){
            gmail.users.messages.list({
                userId: 'me',
                q: params
            }, function(err, res){
                if(err) reject(err);
                resolve(res.messages);
            });
        });
}

function getMessage(messages){
    var gmail = google.gmail('v1');
    var msgs = [];

    return new Promise(
        function(resolve, reject){
            _.each(messages, function(message){
                gmail.users.messages.get({
                    userId: 'me',
                    id: message.id
                }, function(err, response){
                    if(err) reject(err);

                    var pdfMime = _.find(response.payload.parts, {mimeType: 'application/pdf'});
                    msgs.push({messageId: message.id, attachmentId: pdfMime.body.attachmentId});

                    if(msgs.length === messages.length) resolve(msgs);
                })
            });
        }
    )
}

function getAttachment(messages) {
    var gmail = google.gmail('v1');
    var buffers = [];

    return new Promise(
        function(resolve, reject){
            _.each(messages, function(data){
                gmail.users.messages.attachments.get({
                    userId: 'me',
                    messageId: data.messageId,
                    id: data.attachmentId
                }, function(err, response){
                    if(err) reject(err);

                    buffers.push(new Buffer(response.data, 'base64'));
                    if(buffers.length === messages.length) resolve(buffers);
                });
            })
        }
    )
}

function parseBufferToJson(buffers){
    var arr = [];
    return new Promise(
        function(resolve, reject){
            _.each(buffers, function(buffer){
                var pdfParser = new PDFParser();
                pdfParser.parseBuffer(buffer);

                pdfParser.on("pdfParser_dataReady", (pdfData) => {
                    if(!pdfData) return;

                    arr.push(pdfData);
                    if(buffers.length === arr.length) resolve(arr);
                });
            });

        }
    )
}

function parsePdfJsonData(pdfArray){
    return new Promise(
        function (resolve, reject) {
            var arr = mapPagesToPdfArray(pdfArray);
            arr = mapTextsToPdfArray(arr);
            arr = mapStringsToPdfArray(arr);
            arr = cleanUnecessaryStrings(arr);
            arr = sortCorrectlyByDateAndTime(arr);

            resolve(arr);

            function mapPagesToPdfArray(pdfArray){
                return  _.map(pdfArray, function(pdf){
                    return _.get(pdf, 'formImage.Pages');
                });
            }

            function mapTextsToPdfArray(pdfArray){
                return _.map(pdfArray, function(pdf){
                    return _.flatMap(pdf, function(page){
                        return page.Texts;
                    });
                });
            }

            function mapStringsToPdfArray(pdfArray){
                return _.map(pdfArray, function(pdf){
                    return _.flatMap(pdf, function(text){
                        return decodeURIComponent(_.get(text, 'R[0].T'));
                    });
                })
            }

            function cleanUnecessaryStrings(pdfArray){
                var f = _.flatten(pdfArray);
                var eIdx = [];

                var sIdx = _.reduce(f, function(a, e, i){
                    if(e === 'Name:') a.push(i)
                    if(e === 'Rate:') eIdx.push(i+1)
                    return a;
                }, [])

                return _.map(_.zip(sIdx, eIdx), function(idxs){
                    var toPull = ['Name:', 'ID Number:', 'Location:', 'Facility:', 'Date:', 'Time slot:'];
                    var s = f.slice(idxs[0], idxs[1]);

                    var toPullIdxs = _.map(toPull, function(str) {
                        return _.indexOf(s, str)+1;
                    });
                    return _.pullAt(s, toPullIdxs);
                });

            }

            function sortCorrectlyByDateAndTime(pdfArray){
                var grouped = _.groupBy(pdfArray, function(o){ return o[4];});
                var arr = _.map(grouped, function(grp){
                     return _.sortBy(grp, function(o){
                          return o[5];
                     });
                });

                return _.flatten(arr);
            }
        }
    )
}
