var fs = require('fs');
var makeGmailApiCall = require('./src/getDataFromEmailAttachments');
var auth = require('./src/auth');
var path = require('path');
var token = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'client_secret.json'), 'utf8'));

auth.authorize(token)
    .then(makeGmailApiCall, auth.getNewToken)
    .then(function(data){
        fs.writeFileSync(path.resolve(__dirname, 'schedule.json'), JSON.stringify(data));
    })
